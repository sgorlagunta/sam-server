const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const mlogin = require('../models/uploadmanagers');


// Authenticate
router.post('/', (req, res, next) => {
  const email = req.body.email;

console.log('from server: '+email);


  mlogin.getManagerByUsername(email, (err, manager) => {
    if(err) {
      console.log('err: '+err);
       throw err;
     }
    if(!manager){
      console.log('Not found user server: '+manager);
      return res.json({success: false, msg: 'User not found'});
    }
    else {
        console.log('found user server: '+manager);
      return res.json({success: true, msg: 'User found'});
    }


  });
});

module.exports = router;
