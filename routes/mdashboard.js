var express = require('express');
var router = express.Router();
const config = require('../config/database');
const dashboard = require('../models/managerextract');
const managerchange = require('../models/managerchange');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

router.get('/', function(req, res){
  dashboard.getManagerDashboard(function(err,usersDashboard){
      if(err) throw err;
      res.json(usersDashboard);
 });
});

router.get('/:manager', function(req, res){
  //console.log("from server client::"+manager);
  console.log("from server request params::"+req.params.manager);
  //return res.json({success: true, msg:'Upload Completed'});
  var umanager = req.params.manager;
  //umanager = umanager.toUpperCase();
  console.log("umanager:;"+umanager);
  umanager = umanager.toUpperCase();
  console.log("umanager:;"+umanager);
  dashboard.getManagerExtractsClients(umanager,function(err,primarymanagers){
      if(err) throw err;
      res.json(primarymanagers);
 });

 });



module.exports = router;
