var express = require('express');
var router = express.Router();
const config = require('../config/database');
const applications = require('../models/application');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  console.log("serveR: set header is done  ");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var DIR = './uploads/';

router.use(bodyParser.json());
var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});
var upload = multer({ //multer settings
  storage: storage
}).single('photo');

//define the type of upload multer would be doing and pass in its destination, in our case, its a single file with the name photo
//var upload = multer({dest: DIR}).single('single');

//router.get('/', function(req, res, next) {
// render the index page, and pass data to it.
//  res.render('index', { title: 'Express' });
//});

router.get('/', function(req, res){
     applications.getApplications(function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
    });
    // return res.json({success: false, msg: 'Wrong password'});
 });

//our file upload function.
router.post('/', function (req, res, next) {
     var path = '';

     upload(req, res, function (err) {
        if (err) {
          // An error occurred when uploading
          console.log(err);
          return res.status(422).send("an Error occured")
        }
       // No error occured.
        path = req.file.path;
        //return res.send("Upload Completed for "+path);

        /** Multer gives us file info in req.file object */
        if(!req.file){
          res.json({error_code:1,err_desc:"No file passed"});
          return;
        }

            /** Check the extension of the incoming file and
        *  use the appropriate module
        */
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
          exceltojson = xlsxtojson;
        } else {
          exceltojson = xlstojson;
        }
        console.log(req.file.path);

        try {
          exceltojson({
            input: req.file.path,
            output: null, //since we don't need output.json
            lowerCaseHeaders:true
          }, function(err,result){
            if(err) {
              return res.json({error_code:1,err_desc:err, data: null});
            }
            var XLSX = require('xlsx');
        var workbook = XLSX.readFile(req.file.path);
        var sheet_name_list = workbook.SheetNames;
        sheet_name_list.forEach(function(y) {
          var worksheet = workbook.Sheets[y];
          var headers = {};
          var data = [];
          for(z in worksheet) {
            if(z[0] === '!') continue;
            //parse out the column, row, and value
            var col = z.substring(0,1);
            var row = parseInt(z.substring(1));
            var value = worksheet[z].v;

            //store header names
            if(row == 1) {
              headers[col] = value;
              continue;
            }

            if(!data[row]) data[row]={};
            data[row][headers[col]] = value;
          }
          //drop those first two rows which are empty
          data.shift();
          data.shift();
          console.log(data);
          console.log(data.length);

          for(var i = 0; i < data.length; i++){
            //  console.log(data[i].Manager);
            var newApplication = {
              applicationName: data[i].Application,
              applicationOwner : data[i].ApplicationOwner,
              bu : data[i].BU,
              platform: data[i].Platform,
              classification: data[i].Classification,
              scope: data[i].Scope,
              poc: data[i].POC,
              comments: data[i].Comments
            }
            applications.addApplications(newApplication,function(err,newApplication){
              if(err) throw err;
            });
                //console.log('saved record'+i);
              }
            });
            // res.json({error_code:0,err_desc:null, data: result});
          });
        //  res.json({error_code:0,err_desc:null, data: result});
        } catch (e){
          res.json({error_code:1,err_desc:"Corupted excel file"});
        }
  });
return res.json({success: true, msg:'Upload Completed'});
});

router.delete('/:_id', function(req, res){

    applications.deleteApplication(req.params._id ,  function(err,primaryapplications){
        if(err) throw err;
        res.json(primaryapplications);
    });
});

router.get('/:_id', function(req, res){

    applications.getApplication(req.params._id , function(err,primaryapplications){
        if(err) throw err;
        res.json(primaryapplications);
    });
});

router.put('/:_id', function(req, res){
    var update = {
       applicationName: req.body.applicationName,
       applicationOwner : req.body.applicationOwner,
       bu : req.body.bu,
       platform: req.body.platform,
       classification: req.body.classification,
       scope: req.body.scope,
       poc: req.body.poc,
       comments: req.body.comments
   }
    applications.updateApplication(req.params._id , update, function(err,primaryapplications){
        if(err) throw err;
        res.json(primaryapplications);
    });
});


module.exports = router;
