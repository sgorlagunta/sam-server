var express = require('express');
var router = express.Router();
const config = require('../config/database');
const applications = require('../models/application');
const path = require('path');
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");


router.get('/', function(req, res){
     applications.getApplications(function(err,primaryapplications){
         if(err) throw err;
         res.json(primaryapplications);
    });
    // return res.json({success: false, msg: 'Wrong password'});
 });

//our file upload function.
router.post('/', function (req, res, next) {
  var newPrimaryApplications = {
      applicationName: req.body.applicationName,
      applicationOwner : req.body.applicationOwner,
      bu : req.body.bu,
      platform: req.body.platform,
      classification: req.body.classification,
      scope: req.body.scope,
      poc: req.body.poc,
      comments: req.body.comments
  }
   applications.addApplication(newPrimaryApplications,function(err,primaryapplications){
       if(err) throw err;
       res.json(primaryapplications);
   });

});


module.exports = router;
