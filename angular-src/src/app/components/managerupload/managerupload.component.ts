import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import {UploadmanagerService} from '../../services/uploadmanager.service';
import { Uploadmanager } from './uploadmanager'

@Component({
  selector: 'app-managerupload',
  templateUrl: './managerupload.component.html',
  styleUrls: ['./managerupload.component.css']
})
export class ManageruploadComponent implements OnInit {

  constructor( public uploadmanagerService:UploadmanagerService,
   public route:ActivatedRoute,
   public router:Router) { }

   ngOnInit() {
     this.getUploadmanagers();
 }
 public uploader:FileUploader = new FileUploader({url: "http://localhost:8080/api/uploadmanager/"});

 primarymanagers:Uploadmanager;
 getUploadmanagers(){
   this.uploadmanagerService.getUploadmanagers()
       .subscribe(primarymanagers=>{
         this.primarymanagers = primarymanagers;
         console.log(this.primarymanagers);
       });
 }

 goBack(){
   this.router.navigate(['/mhome'])
 }


}
