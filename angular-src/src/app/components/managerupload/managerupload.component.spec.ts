import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageruploadComponent } from './managerupload.component';

describe('ManageruploadComponent', () => {
  let component: ManageruploadComponent;
  let fixture: ComponentFixture<ManageruploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageruploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageruploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
