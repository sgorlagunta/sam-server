import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerexxtractComponent } from './managerexxtract.component';

describe('ManagerexxtractComponent', () => {
  let component: ManagerexxtractComponent;
  let fixture: ComponentFixture<ManagerexxtractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerexxtractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerexxtractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
