import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';
import {Ng2PaginationModule} from 'ng2-pagination';

import { ActivatedRoute, Params, Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import { ExcelService } from '../../services/excel.service';
import { UsersDashboard,Dashboard } from './dashboard';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { DashboardService } from '../../services/dashboard.service';
import {ManagerExtract} from '../../models/managerextract';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {DialogComponent,DialogService} from 'ng2-bootstrap-modal';
//import { ModalDirective } from 'ngx-bootstrap';


@Component({
  selector: 'app-managerresponses',
  templateUrl: './managerresponses.component.html',
  styleUrls: ['./managerresponses.component.css']
})
export class ManagerresponsesComponent implements OnInit {

   //usersDashboard:Object;
   primarymanagers:any[];
   usersDashboard:any[];//UsersDashboard;
   apps:any[];
   apps2:any[];//Dashboard;
   //Dashboard;//any[];
   //apps2 = new Dashboard();
   //apps2:Object;
   getDashboardData;


 ngOnInit() {
 }
  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private dashboardService:DashboardService,
         private excelService: ExcelService,
         //public ngxSmartModalService:NgxSmartModalService,
         //public dialogService:DialogService,
         private router: Router) {
          // super(dialogService);
           this.getManagerDashboard();
           this.excelService = excelService;
         }

       getManagerDashboard(){
                this.dashboardService.getManagerDashboard()
                    .subscribe(usersDashboard=>{
                      this.getDashboardData = usersDashboard;
                      var len = usersDashboard.length;
                      if(len>0){
                      console.log("Implement filtering functionality here"+usersDashboard.length);
                      var result = [];
                      var item = [];
                      var result = [];

                      var application = [];
                      var total = new Array(len).fill(0);//[] = 0;// [0];
                      var recertified= new Array(len).fill(0);
                      var expired= new Array(len).fill(0);
                      var pending= new Array(len).fill(0);
                      var removals= new Array(len).fill(0);
                      var leavers= new Array(len).fill(0);
                      var noChange= new Array(len).fill(0);
                      var movedToSpecial= new Array(len).fill(0);
                      var noLongerAssigned= new Array(len).fill(0);

                      var ttotal = 0;
                      var trecertified= 0;
                      var texpired= 0;
                      var tpending= 0;
                      var tremovals= 0;
                      var tleavers= 0;
                      var tnoChange= 0;
                      var tmovedToSpecial= 0;
                      var tnoLongerAssigned= 0;


                      //var apps2 = new Dashboard();

                      var p = 0;
                      for(var k=0;k<len;k++) {
                        item[k] = usersDashboard[k].manager;
                        //console.log(usersDashboard[k].application);
                      }
                      // console.log(" item length"+len);

                       for(var i=0;i<len;i++){
                                  var isDistinct = false;
                                  for(var j=0;j<i;j++){
                                      if(item[i] == item[j]){
                                          isDistinct = true;
                                          break;
                                      }
                                  }
                                  if(!isDistinct){
                                    result[p++] = item[i];
                                    //  console.log(item[i]+" ");
                                  }
                              }


                      for(var i=0;i<result.length;i++){
                          var appl;
                          var sta;
                          for(var k=0;k<len;k++) {
                           appl = usersDashboard[k].manager;
                           sta = usersDashboard[k].status;
                             if(result[i] == appl )
                             {
                               total[i]++;ttotal++;
                               if(sta == 'Expired') {
                                expired[i]++;texpired++;recertified[i]++;trecertified++;}
                               else  if(sta == 'Not Recertified') {
                                pending[i]++; tpending++; }
                               else  if(sta == 'Leaver') {
                                leavers[i]++;tleavers++;recertified[i]++;trecertified++; }
                               else  if(sta == 'Removal') {
                                removals[i]++;tremovals++;recertified[i]++;trecertified++;}
                               else  if(sta == 'No Change') {
                                noChange[i]++;tnoChange++;recertified[i]++;trecertified++;}
                              else  if(sta == 'No Longer the Manager for User') {
                                 noLongerAssigned[i]++;tnoLongerAssigned++;recertified[i]++;trecertified++;}
                               else  if(sta == 'Moved to Special Cycle') {
                                movedToSpecial[i]++;tmovedToSpecial++;recertified[i]++;trecertified++;}
                                //else  if(sta != 'Not Recertified') {
                                 //recertified[i]++;trecertified++;}
                             }
                           }
                        }

                      //  delete usersDashboard.manager;
                      //  usersDashboard["manager"]=null;
                      //  delete usersDashboard["manager"];

                        //console.log(usersDashboard.manager);
                        this.apps = usersDashboard;
                        console.log("  this.apps :: keys"  +this.apps.keys());



                        for (let i in this.apps) {
                          this.apps[i]._id = undefined;
                          this.apps[i].manager = undefined;
                          this.apps[i].logonid = undefined;
                          this.apps[i].status = undefined;
                          this.apps[i].surname = undefined;
                          this.apps[i].initials = undefined;
                          this.apps[i].userlocation = undefined;
                          this.apps[i].comments = undefined;
                          this.apps[i].dmanager = undefined;
                          this.apps[i].application = undefined;
                        }
                        for (var key in this.apps){
                          //console.log( "this.apps: keys"+key, this.apps[key] );
                          delete this.apps[key]._id;
                          delete this.apps[key].manager;
                          delete this.apps[key].logonid;
                          delete this.apps[key].status;
                          delete this.apps[key].surname;
                          delete this.apps[key].initials;
                          delete this.apps[key].userlocation;
                          delete this.apps[key].comments;
                          delete this.apps[key].dmanager;
                          delete this.apps[key].application;
                          //console.log( "this.apps: keys"+key, this.apps[key] );
                        }
                        //console.log(delete this.apps["manager"]);
                       /*delete this.apps["manager"];
                        this.apps["manager"]=null;
                        this.apps["_id"] = null;
                        this.apps["logonid"] = null;
                        this.apps["status"] = null;
                        this.apps["surname"] = null;
                        this.apps["initials"] = null;
                        this.apps["userlocation"] = null;
                        this.apps["comments"] = null;*/
                        //this.apps = [];
                        this.apps.length=(result.length+1);
                        console.log("this.apps.length::"+this.apps.length);
                        var t = 0;
                        if(this.apps.length>1) {
                        for (let i in this.apps) {
                        t++;
                        if(t <= result.length)
                        {
                            this.apps[i].manager = result[i];
                            this.apps[i].total = total[i];
                            this.apps[i].pending = pending[i];
                            this.apps[i].recertified = recertified[i];
                            this.apps[i].expired = expired[i];
                            this.apps[i].removals = removals[i];
                            this.apps[i].leavers = leavers[i];
                            this.apps[i].noChange = noChange[i];
                            this.apps[i].movedToSpecial = movedToSpecial[i];
                            this.apps[i].noLongerAssigned = noLongerAssigned[i];
                          /*  this.apps[i]._id = undefined;
                            this.apps[i].manager = undefined;
                            this.apps[i].logonid = undefined;
                            this.apps[i].status = undefined;
                            this.apps[i].surname = undefined;
                            this.apps[i].initials = undefined;
                            this.apps[i].userlocation = undefined;
                            this.apps[i].comments = undefined; */
                        }
                        else {
                            //this.apps[i] = 0;
                            this.apps[i].manager = "Total Counts";
                            this.apps[i].total = ttotal;
                            this.apps[i].expired = texpired;
                            this.apps[i].recertified = trecertified;
                            this.apps[i].pending = tpending;
                            this.apps[i].leavers = tleavers;
                            this.apps[i].removals = tremovals;
                            this.apps[i].noChange = tnoChange;
                            this.apps[i].movedToSpecial = tmovedToSpecial;
                            this.apps[i].noLongerAssigned = tnoLongerAssigned;
                        /*    this.apps[i]._id = undefined;
                            this.apps[i].manager = undefined;
                            this.apps[i].logonid = undefined;
                            this.apps[i].status = undefined;
                            this.apps[i].surname = undefined;
                            this.apps[i].initials = undefined;
                            this.apps[i].userlocation = undefined;
                            this.apps[i].comments = undefined; */
                        }
                        }
                      }
                    }
                  });
              }
        exportToExcel(event) {
            
          console.log(this.apps.toString);
          var json = [];
          for(let i in this.apps){
            var update = {
              Manager: this.apps[i].manager,
              Pending: this.apps[i].pending,
              Recertified: this.apps[i].recertified,
              Expired: this.apps[i].expired,
              Removals: this.apps[i].removals,
              Leavers: this.apps[i].leavers,
              No_Change: this.apps[i].noChange,
              Moved_To_Special_Cycle: this.apps[i].movedToSpecial,
              No_Longer_the_Manager_for_User: this.apps[i].noLongerAssigned,
          }
            json.push(update);
         }
              this.excelService.exportAsExcelFile(json, 'ManagerResponses');
        }

        ManagerReponse(mgr)
        {
            console.log("mgr::"+mgr);
            this.getManagerExtracts(mgr)
            //var temp = <html><h1>"+mgr+"</h1></html>;
            //var person = alert(temp);
           //var oldmanager = (<HTMLInputElement>document.getElementById('dialog'));
            //var t = document.getElementById("dialog");
          //  oldmanager.innerHTML = "<html><h1>"+mgr+"</h1></html>";
          /*  alert('Below are the extracts of Manager:: '+mgr + '\n'
                   + '\n\n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                   + '1. managername \t user email  \t status \n'
                 ); */
                    //oldmanager.();
            //  if (person == null || person == "") {
            //      txt = "User ca
            //var newwindow = "location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes";
            //var temp = mgr;
            //window.open(temp,"_blank",newwindow);
            //confirm("Are you sure want to submit responses:: "+mgr);
        }

        getManagerExtracts(mgr){
             //alert(mgr+":: list of users");
             this.dashboardService.getManagerExtracts(mgr)
                  .subscribe(primarymanagers=>{
                   this.primarymanagers = primarymanagers;
                    /* ==========This is not requiered because we are using bootstrap modal  ===========          
                    var len = this.primarymanagers.length;
                    if(len==0){
                    alert("No users found for manager ::"+mgr);
                  }
                  else{
                    var ScreenWidth = window.screen.width;
                    var ScreenHeight = window.screen.height;
                    var movefromedge=0;
                    var placementx = (ScreenWidth/2)-((600)/2);
                    var placementy = (ScreenHeight/2)-((400)/2);
                    var WinPop;
                    WinPop = window.open("About:Blank","","width=800,height=400,toolbar=0,location=0,directories=0,status=0,scrollbars=1,menubars=0,resizable=1,left="+placementx+",top="+placementy+",screenX="+placementx+",screenY="+placementy);
                    var SayWhat = "";//"Hello User ::"+mgr;

                    SayWhat = '<table border="1">';
                    var message = "<thead><tr><th>S.No</th><th>Surname</th><th>LogonID</th><th>Application</th><th>Status</th></tr></thead><tbody>";

                    len = this.primarymanagers.length;
                    //alert("length2 of users::"+len);
                    for(var i=0;i<len;i++){
                    // for (let i in this.primarymanagers) {
                      //alert("mana::"+this.primarymanagers[i].manager);
                      //alert("this.primarymanagers"+this.primarymanagers);
                      //alert("primarymanagers"+primarymanagers);
                      message = message + "<tr><td>" + (i+1) +" </td><td>"
                      //+ this.primarymanagers[i].manager+ "\t"
                      + this.primarymanagers[i].surname+ "</td><td>"
                      //+ this.primarymanagers[i].initials+ "\t"
                      + this.primarymanagers[i].logonid+ "</td><td>"
                      //+ this.primarymanagers[i].userlocation+ "\t"
                      + this.primarymanagers[i].application+ "</td><td>"
                      + this.primarymanagers[i].status+ "</td></tr>";
                    }
                    // alert(message);
                    var last = '</tbody></table><button type="button" onclick="window.close()">Close</button>';

                    WinPop.document.write('<html>\n<head><h2>'+mgr+'</h2></head>\n<body>'+SayWhat+message+last+'</body></html>');
                  }*/
            });
      }

      /* exportToExcel()
        {
          const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          const EXCEL_EXTENSION = '.xlsx';
          const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.apps);
          const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
          const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' })
          //var htmltable= document.getElementById('report');
          //var html = htmltable.innerHTML;
          var fileName = "PrimaryDashboard";
           const data: Blob = new Blob([excelBuffer], {
            type: EXCEL_TYPE
          });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
          //window.open('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' + encodeURIComponent(html));
          //window.open('data:text/csv;charset=utf-8;' + encodeURIComponent(html));
   //text/csv;charset=utf-8;
           //var blob = new Blob([document.getElementById('report').innerHTML], {
           //  type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8" });
           //  saveAs(blob, "report.xls");
          console.log("download button clicked");
      }*/   
  }
