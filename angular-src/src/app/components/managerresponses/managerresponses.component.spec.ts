import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerresponsesComponent } from './managerresponses.component';

describe('ManagerresponsesComponent', () => {
  let component: ManagerresponsesComponent;
  let fixture: ComponentFixture<ManagerresponsesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerresponsesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerresponsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
