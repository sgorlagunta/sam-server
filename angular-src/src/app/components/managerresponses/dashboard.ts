export class UsersDashboard{
  manager:String;
  surname:String;
  initials:String;
  logonid: String;
  userlocation:String;
  application: String;
  status: String;
  commments: String;
}

export class Dashboard{
  application:String;
  total:String;
  recertified:String;
  expired: String;
  pending:String;
  removals: String;
  leavers: String;
  noChange: String;
  movedToSpecial: String;
  noLongerAssigned: String;
}
