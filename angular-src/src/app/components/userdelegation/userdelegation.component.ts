import { Component, OnInit, Directive, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import {SearchFilterPipe} from '../../services/searchfilter';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import {SelectModule} from 'angular2-select';

import {Product} from '../../models/product';
import {Country} from '../../models/country';

import { ExcelService } from '../../services/excel.service';


@Component({
  selector: 'app-userdelegation',
  templateUrl: './userdelegation.component.html',
  styleUrls: ['./userdelegation.component.css']
})
export class UserdelegationComponent implements OnInit {

  //primarymanagers:ManagerExtract;
  title = 'Assign New Manager';
  primarymanagers:any[];
  statusOptions: any[] = [{ status: 'Not Recertified' }, { status: 'No Change' }, { status: 'Leavers' }, { status: 'Removals' }];
  userFilter: any = ({ manager: ''} || {resource:''} || {initials:''} || {userlocation:''} || {application:''} || {status:''} );
Games = [{name:"Not Recertified",type:"Not Recertified"},
{name:"No Change",type:"No Change"},
{name:"Leavers",type:"Leavers"},
{name:"Removals",type:"Removals"}];



//filter: ManagerExtract = new ManagerExtract();

public statusAll = [
    { value: 'NC', display: 'No Change' },
    { value: 'R', display: 'Removal' },
    { value: 'L', display: 'Leaver' }
];

public products: Product[] = [
      { "id": 1, "name": "Not Recertified" },
      // { "id": 2, "name": "No Change" },
      // { "id": 3, "name": "Leaver" },
      // { "id": 4, "name": "Removal" },
      { "id": 2, "name": "Moved to Special Cycle" },
      { "id": 3, "name": "No Longer the Manager for User" }
    ];
    public selectedProduct: Product = this.products[0];
    onSelect() {
        this.selectedProduct = null;
        //console.log(this.products.name);
        //for (var i = 0; i < this.products.length; i++)
        //{
          //if (this.products[i].id == productId) {
          //  this.selectedProduct = this.products[i];
          //}
        //}
    }


  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         public route:ActivatedRoute,
         private excelService: ExcelService,
         private router: Router) {
          this.getManagerExtracts();
          this.excelService = excelService;
          //this.onChange("select");
        //  this.games = [{"name":"Not Recertified","type":"Not Recertified"},
        //  {"name":"No Change","type":"No Change"},
        //  {"name":"Leavers","type":"Leavers"},
        //  {"name":"Removals","type":"Removals"}];
       }

  ngOnInit() {
    this.getManagerExtracts();
    //this.onChange("select");
}

/* This is not required
setToRentControl(value){
    //this.vm.toRent.updateValue(value);
    //alert(value); //true/false
    if(value=='R')
    {
      //alert("seletced Removals");
      //this.products = [{ "id": 4, "name": "Removals" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'Removal';
      }

    }
    else if(value=='NC')
    {
      //alert("seletced No Change");
      //this.products = [{ "id": 2, "name": "No Change" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'No Change';
      }
    }
    else if(value=='L')
    {
      //alert("seletced Leavers");
      //this.products = [  { "id": 3, "name": "Leavers" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'Leaver';
      }
    }
    else
    {
      //alert("seletced None");
      this.products =  [
            { "id": 1, "name": "Not Recertified" },
            { "id": 2, "name": "No Change" },
            { "id": 3, "name": "Leaver" },
            { "id": 4, "name": "Removal" },
            { "id": 5, "name": "Moved to Special Cycle" },
            { "id": 6, "name": "No Longer the Manager for User" }
          ];
    }
}*/

showSelectValue(mySelect) {
    console.log(mySelect);
}

clearSearch()
{
  window.location.reload();
}


  id = this.route.snapshot.params['id'];

  selectedcheck = true;
  valuecheck: boolean;
  checkall: boolean;

  //this.selectedProduct = this.route.snapshot.params['id'];
  //primarymanagersList : ({});

selected1 = {};

selectAll(){

  var x = (<HTMLInputElement>document.getElementById("all")).checked;
  //document.getElementById("all").checked;
  console.log("value of x::"+x);
  var len = this.primarymanagers.length;
  if(x==true)
  {
      for (var j=0; j<len; j++) {
        console.log(this.primarymanagers[j]._id);
        var ids = this.primarymanagers[j]._id;
        console.log(ids);
        console.log(j);
        //var = "all"+[i];
        if((<HTMLInputElement>document.getElementById(ids))) {
          console.log("in if loop for j = "+j);
          (<HTMLInputElement>document.getElementById(ids)).checked = true;
      }
      }
    }
    else {
            for (var j=0; j<len; j++) {
              console.log(this.primarymanagers[j]._id);
              var ids = this.primarymanagers[j]._id;
              console.log(ids);
              console.log(j);
            if((<HTMLInputElement>document.getElementById(ids))){
              console.log("in if loop for j = "+j);
              (<HTMLInputElement>document.getElementById(ids)).checked = false;
          }
          }
        }

/*  console.log("itemID::");
  for (var i = 0; i < this.primarymanagers.length; i++) {
    var item = this.primarymanagers[i]._id;
    console.log("itemID::"+item);
     this.selected1 = true;
     console.log("itemID manager::"+this.selected1);
     (<HTMLInputElement>document.getElementById(item)).checked = true;
} */
}





onchanges(){
  (<HTMLInputElement>document.getElementById("all")).checked = false;
}


  onChange() {
  //this.valuecheck = JSON.parse(values,valuecheck);
var x = (<HTMLInputElement>document.getElementById("all")).checked;
//document.getElementById("all").checked;
console.log("value of x::"+x);
var len = this.primarymanagers.length;
if(x==true)
{
    for (var j=0; j<len; j++) {
      console.log(this.primarymanagers[j]._id);
      var ids = this.primarymanagers[j]._id;
      console.log(ids);
      console.log(j);
      //var = "all"+[i];
      if((<HTMLInputElement>document.getElementById(ids))) {
        console.log("in if loop for j = "+j);
        (<HTMLInputElement>document.getElementById(ids)).checked = true;
    }
    }
}
else {
        for (var j=0; j<len; j++) {
          console.log(this.primarymanagers[j]._id);
          var ids = this.primarymanagers[j]._id;
          console.log(ids);
          console.log(j);
        if((<HTMLInputElement>document.getElementById(ids))){
          console.log("in if loop for j = "+j);
          (<HTMLInputElement>document.getElementById(ids)).checked = false;
      }
      }
}
/*  var checkboxes = document.getElementsByName("all");
    var checkboxesChecked = [];
    // loop over them all
    for (var i=0; i<checkboxes.length; i++) {
       // And stick the checked ones onto an array...
       if (checkboxes[i].checked) {
          checkboxesChecked.push(checkboxes[i]);
       }
    } */

    //console.log("check box::"+checkboxesChecked.length > 0 ? checkboxesChecked : null);





    //console.log(this.valuecheck);
    //this.valuecheck = valuecheck;
    //console.log("values:"+this.valuecheck);
    //var myCheckboxes = document.getElementById('checkbox');
    //console.log("checkbox values::"+document.getElementById('checkbox'));
    //console.log(this.primarymanagers.length);
    //for (let i in this.primarymanagers) {
    //  var oldmanager = (<HTMLInputElement>document.getElementById('all'+[i]));
    //  console.log("oldmanager::"+oldmanager);
    //}


    //myCheckboxes = event.target.checked;
  }


  // $("input[type=checkbox].selectedcheck");
  check = []
 arrayCheck = [{'key11':'value11'},{'key11':'value12'},{'key11':'value13'},{'key11':'value14'},{'key11':'value15'}];
 demo2 = [1,2,3,4,5]
 demoChk= [];

  updateChecked2(value,event){
      if(event.target.checked){
        this.demoChk.push(value);
      }
      else if (!event.target.checked){
        let indexx = this.demoChk.indexOf(value);
        this.demoChk.splice(indexx,1);
      }
      console.log(this.demoChk)
    }



  saveResponse(){
    console.log("in save response");
    console.log(this.primarymanagers);
    //var myCheckboxes = document.getElementById('selectedcheck');

    console.log("  this.demoChk::"+  this.demoChk);
    console.log("  this.demoChk::"+  this.demoChk.length);

if(this.demoChk.length>0){
  var txt;

  var person = prompt("Please enter Manager Email ID to assign selected users like mail@domain.com", "");

    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        txt = "Hello " + person + "! How are you today?";
        if(confirm("Are you sure want to submit responses ")) {
              console.log("Implement delete functionality here");
          for (let i in this.primarymanagers) {
          console.log("  this.demoChk::"+  this.demoChk);
          console.log("  this.demoChk::"+  this.demoChk.length);
          for(let j in this.demoChk)
          {
            if(this.demoChk[j]==this.primarymanagers[i]._id)
            {
              var update = {
                 _id: this.primarymanagers[i]._id,
                 manager: person.toUpperCase(),
                 resource : this.primarymanagers[i].resource,
                 initials : this.primarymanagers[i].initials,
                 logonid: this.primarymanagers[i].logonid,
                 region: this.primarymanagers[i].region,
                 application: this.primarymanagers[i].application,
                 status:   "Not Recertified",
                 dmanager:   this.primarymanagers[i].dmanager,
                 comments:   this.primarymanagers[i].comments
                }
                if(update!=null && update!=undefined){
                      this.managerextractService.updateResponses(update._id,update)
                      .subscribe(()=> this.goBack());
                }
             }
           }
        }
        this.flashMessage.show('Status submitted successfully', {cssClass: 'alert-success', timeout: 1000});              //this.router.navigate(['/output']);
                //window.location.reload();
    }
  }
  console.log("Entered manager id::"+txt)
}
else {
  var x = (<HTMLInputElement>document.getElementById("all")).checked;
  //document.getElementById("all").checked;
  console.log("value of x::"+x);
  var len = this.primarymanagers.length;
  if(x==true)
  {
    var txt1;
    var person1 = prompt("Please enter Manager Email ID to assign selected users like mail@domain.com", "");
    if (person1 == null || person1 == "") {
          txt1 = "User cancelled the prompt.";
      } else {
          txt1 = "Hello " + person1 + "! How are you today?";
          if(confirm("Are you sure want to submit responses ")) {
                console.log("Implement delete functionality here");
            for (let i in this.primarymanagers) {
              var update = {
                   _id: this.primarymanagers[i]._id,
                   manager: person1.toUpperCase(),
                   resource : this.primarymanagers[i].resource,
                   initials : this.primarymanagers[i].initials,
                   logonid: this.primarymanagers[i].logonid,
                   region: this.primarymanagers[i].region,
                   application: this.primarymanagers[i].application,
                   status:   "Not Recertified",
                   dmanager:   this.primarymanagers[i].dmanager,
                   comments:   this.primarymanagers[i].comments
                  }

                  //console.log("Length::"+i+"::"+update);
                 this.managerextractService.updateResponses(update._id,update)
                        .subscribe(()=> this.goBack());
              }
              this.flashMessage.show('Status submitted successfully', {cssClass: 'alert-success', timeout: 1000});
      }
    }
  }
 }
}

  goBack(){
      window.location.reload();
  //this.router.navigate(['output'])

         //this.router.navigate(['/output']);
          //window.location.reload();
  }





  getManagerExtracts(){
     this.managerextractService.getManagerExtractsUsers()
          .subscribe(primarymanagers=>{
            this.primarymanagers = primarymanagers;
        });
    }

    public items:Array<string> = ['Not Recertified','Leavers','Removals','No Change'];

    private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;

  private get disabledV():string {
    return this._disabledV;
  }

  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }

  public selected(value:any):void {
    console.log('Selected value is: ', value);
  }

  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }

  public typed(value:any):void {
    console.log('New search input: ', value);
  }

  public refreshValue(value:any):void {
    this.value = value;
  }

  exportToExcel(event) {
    this.excelService.exportAsExcelFile(this.primarymanagers, 'output');
  }

}
