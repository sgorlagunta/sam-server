import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdelegationComponent } from './userdelegation.component';

describe('UserdelegationComponent', () => {
  let component: UserdelegationComponent;
  let fixture: ComponentFixture<UserdelegationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserdelegationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserdelegationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
