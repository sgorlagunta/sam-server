import { Component, OnInit } from '@angular/core';
import {ChangemanagerService} from '../../services/changemanager.service';
import { ActivatedRoute, Params, Router } from '@angular/router';


@Component({
  selector: 'app-changemanager',
  templateUrl: './changemanager.component.html',
  styleUrls: ['./changemanager.component.css']
})

export class ChangemanagerComponent implements OnInit {
  constructor(
  public changemanagerService:ChangemanagerService,
  public route:ActivatedRoute,
  public router:Router
) { }

ngOnInit() {
  this.getManagers();
}
managers:Object;
getManagers(){
  this.changemanagerService.getManagers()
      .subscribe(managers=>{
        this.managers = managers;
      });
}

goBack(){
this.router.navigate(['/changemanager'])
}

}
