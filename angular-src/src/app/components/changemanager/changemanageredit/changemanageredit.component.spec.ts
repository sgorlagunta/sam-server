import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangemanagereditComponent } from './changemanageredit.component';

describe('ChangemanagereditComponent', () => {
  let component: ChangemanagereditComponent;
  let fixture: ComponentFixture<ChangemanagereditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangemanagereditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangemanagereditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
