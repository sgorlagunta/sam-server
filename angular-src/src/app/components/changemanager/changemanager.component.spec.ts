import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangemanagerComponent } from './changemanager.component';

describe('ChangemanagerComponent', () => {
  let component: ChangemanagerComponent;
  let fixture: ComponentFixture<ChangemanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangemanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangemanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
