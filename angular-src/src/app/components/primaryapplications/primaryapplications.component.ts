import { Component, OnInit } from '@angular/core';
import {PrimaryapplicationsService} from '../../services/primaryapplications.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-primaryapplications',
  templateUrl: './primaryapplications.component.html',
  styleUrls: ['./primaryapplications.component.css']
})
export class PrimaryapplicationsComponent implements OnInit {

  applicationName: String;
  applicationOwner: String;
  bu: String;
  platform: String;
  classification: String;
  scope: String;
  extractRecieved: String;


  constructor(
    public primaryapplicationsService:PrimaryapplicationsService,
    public route:ActivatedRoute,
    public router:Router
  ) {
    }

  ngOnInit() {
    this.getPrimaryApplications();
  }

  primaryapplications:Object;
  getPrimaryApplications(){
      this.primaryapplicationsService.getPrimaryApplications()
          .subscribe(primaryapplications=>{
            this.primaryapplications = primaryapplications;
          })
    }

    deletePrimaryApplications(id){
  this.primaryapplicationsService.deletePrimaryApplications(id)
    .subscribe(()=>{
      this.getPrimaryApplications();
    });
}



}
