import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryapplicationsComponent } from './primaryapplications.component';

describe('PrimaryapplicationsComponent', () => {
  let component: PrimaryapplicationsComponent;
  let fixture: ComponentFixture<PrimaryapplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryapplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryapplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
