import { Component, OnInit } from '@angular/core';
import {PrimaryapplicationsService} from '../../services/primaryapplications.service';
import { ActivatedRoute, Params, Router } from '@angular/router';



@Component({
  selector: 'app-primaryapplicationshow',
  templateUrl: './primaryapplicationshow.component.html',
  styleUrls: ['./primaryapplicationshow.component.css']
})
export class PrimaryapplicationshowComponent implements OnInit {

  constructor(
    public primaryapplicationsService:PrimaryapplicationsService,
    public route:ActivatedRoute,
    public router:Router
  ) { }

  ngOnInit() {
    this.getPrimaryApplication();
  }
  primaryapplications:Object;
  getPrimaryApplication(){
    var id = this.route.snapshot.params['id'];
    this.primaryapplicationsService.getPrimaryApplication(id)
        .subscribe(primaryapplications=>{
          this.primaryapplications = primaryapplications;
        })
  }
  goBack(){
  this.router.navigate(['/primaryapplications'])
  }

}
