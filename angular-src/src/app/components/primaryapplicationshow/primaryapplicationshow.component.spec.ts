import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryapplicationshowComponent } from './primaryapplicationshow.component';

describe('PrimaryapplicationshowComponent', () => {
  let component: PrimaryapplicationshowComponent;
  let fixture: ComponentFixture<PrimaryapplicationshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryapplicationshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryapplicationshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
