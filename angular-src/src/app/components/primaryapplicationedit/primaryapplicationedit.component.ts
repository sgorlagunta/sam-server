import { Component, OnInit } from '@angular/core';
import {PrimaryapplicationsService} from '../../services/primaryapplications.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Primaryapplications } from './primaryapplications'

@Component({
  selector: 'app-primaryapplicationedit',
  templateUrl: './primaryapplicationedit.component.html',
  styleUrls: ['./primaryapplicationedit.component.css']
})
export class PrimaryapplicationeditComponent implements OnInit {

  constructor(
    public primaryapplicationsService:PrimaryapplicationsService,
    public route:ActivatedRoute,
    public router:Router
  ) { }

  ngOnInit() {
    this.getPrimaryApplication();
  }

  primaryapplications = new Primaryapplications();
  id = this.route.snapshot.params['id'];

  getPrimaryApplication(){
    this.primaryapplicationsService.getPrimaryApplication(this.id)
        .subscribe(primaryapplications=>{
          this.primaryapplications = primaryapplications;
        })
  }

  updatePrimaryApplications(){
    this.primaryapplicationsService.updatePrimaryApplications(this.id,this.primaryapplications)
        .subscribe(()=> this.goBack())
  }

   goBack(){
    this.router.navigate(['/primaryapplications'])
  }
}
