import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryapplicationeditComponent } from './primaryapplicationedit.component';

describe('PrimaryapplicationeditComponent', () => {
  let component: PrimaryapplicationeditComponent;
  let fixture: ComponentFixture<PrimaryapplicationeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryapplicationeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryapplicationeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
