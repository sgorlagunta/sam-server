import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryapplicationsaddComponent } from './primaryapplicationsadd.component';

describe('PrimaryapplicationsaddComponent', () => {
  let component: PrimaryapplicationsaddComponent;
  let fixture: ComponentFixture<PrimaryapplicationsaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryapplicationsaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryapplicationsaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
