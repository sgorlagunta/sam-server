import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {UploadmanagerService} from '../../services/uploadmanager.service';
import { Uploadmanager } from './uploadmanager';
import {FlashMessagesService} from 'angular2-flash-messages';

@Component({
  selector: 'app-managerdashboard',
  templateUrl: './managerdashboard.component.html',
  styleUrls: ['./managerdashboard.component.css']
})
export class ManagerdashboardComponent implements OnInit {

  constructor( public uploadmanagerService:UploadmanagerService,
   public route:ActivatedRoute,
   private flashMessage:FlashMessagesService,
   public router:Router) { }

  ngOnInit() {
     this.getUploadmanagers();
 }

 primarymanagers:Uploadmanager;
 getUploadmanagers(){
   this.uploadmanagerService.getUploadmanagers()
       .subscribe(primarymanagers=>{
         this.primarymanagers = primarymanagers;
         console.log(this.primarymanagers);
       });
 }

}
