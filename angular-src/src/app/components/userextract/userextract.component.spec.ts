import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserextractComponent } from './userextract.component';

describe('UserextractComponent', () => {
  let component: UserextractComponent;
  let fixture: ComponentFixture<UserextractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserextractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserextractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
