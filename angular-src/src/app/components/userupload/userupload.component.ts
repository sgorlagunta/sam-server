import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import {UploaduserService} from '../../services/uploaduser.service';
import { Uploaduser } from './uploaduser'

@Component({
  selector: 'app-userupload',
  templateUrl: './userupload.component.html',
  styleUrls: ['./userupload.component.css']
})
export class UseruploadComponent implements OnInit {
public uploader:FileUploader = new FileUploader({url: "http://localhost:8080/api/uploaduser/"});
  constructor(
  public uploaduserService:UploaduserService,
   public route:ActivatedRoute,
   public router:Router) {
     this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  var responsePath = JSON.parse(response);
  console.log(response, responsePath);// the url will be in the response
  };
 }

  ngOnInit() {
      this.getUploadusers();
  }


primaryusers:Uploaduser;
getUploadusers(){
  this.uploaduserService.getUploadusers()
      .subscribe(primaryusers=>{
        this.primaryusers = primaryusers;
        console.log(this.primaryusers);
        //this.goBack();
        this.router.navigate(['/uploaduser']);
      });
}
goBack(){
  this.router.navigate(['/uploaduser']);
}


}
