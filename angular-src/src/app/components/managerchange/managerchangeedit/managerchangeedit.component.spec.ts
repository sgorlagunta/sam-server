import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerchangeeditComponent } from './managerchangeedit.component';

describe('ManagerchangeeditComponent', () => {
  let component: ManagerchangeeditComponent;
  let fixture: ComponentFixture<ManagerchangeeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerchangeeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerchangeeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
