import { Component, OnInit, Directive, ElementRef, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import {Ng2PaginationModule} from 'ng2-pagination';

import {ManagerExtract} from '../../models/managerextract';
import {ManagerextractService} from '../../services/managerextract.service';
import {SearchFilterPipe} from '../../services/searchfilter';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import {SelectModule} from 'angular2-select';

import {Product} from '../../models/product';
import {Country} from '../../models/country';

import { ExcelService } from '../../services/excel.service';



@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {
  
  //primarymanagers:ManagerExtract;
  title="Recertification Response"
  filteredToatal = 0;
  primarymanagers:any[];
  statusOptions: any[] = [{ status: 'Not Recertified' }, { status: 'No Change' }, { status: 'Leavers' }, { status: 'Removals' }];
  userFilter: any = ({ manager: ''} || {resource:''} || {initials:''} || {userlocation:''} || {application:''} || {status:''} );
Games = [{name:"Not Recertified",type:"Not Recertified"},
{name:"No Change",type:"No Change"},
{name:"Leavers",type:"Leavers"},
{name:"Removals",type:"Removals"}];
statusselected: any;
//filter: ManagerExtract = new ManagerExtract();

public statusAll = [
    { value: 'NC', display: 'No Change' },
    { value: 'R', display: 'Removal' },
    { value: 'L', display: 'Leaver' },
    { value: 'NA', display: 'No Longer Assigned' }
];

public products: Product[] = [
      { "id": 1, "name": "Not Recertified" },
      { "id": 2, "name": "No Change" },
      { "id": 3, "name": "Leaver" },
      { "id": 4, "name": "Removal" },
      { "id": 5, "name": "Moved to Special Cycle" },
      { "id": 6, "name": "No Longer the Manager for User" }//No Longer Assigned
    ];
    public selectedProduct: Product = this.products[0];
    onSelect() {
        this.selectedProduct = null;
        //console.log(this.products.name);
        //for (var i = 0; i < this.products.length; i++)
        //{
          //if (this.products[i].id == productId) {
          //  this.selectedProduct = this.products[i];
          //}
        //}
    }


  constructor(private http: Http, private el: ElementRef,
         private flashMessage:FlashMessagesService,
         private managerextractService:ManagerextractService,
         public route:ActivatedRoute,
         private excelService: ExcelService,
         private router: Router) {
          this.getManagerExtracts();
          this.excelService = excelService;
        //  this.games = [{"name":"Not Recertified","type":"Not Recertified"},
        //  {"name":"No Change","type":"No Change"},
        //  {"name":"Leavers","type":"Leavers"},
        //  {"name":"Removals","type":"Removals"}];
       }

  ngOnInit() {
    this.getManagerExtracts();
}
myFunction(){
  this.filteredToatal = $("#filter_table_output tbody tr").length;
}
idList = [];
setToRentControl(value){
      var isList = [];
      this.idList = [];
      $("#filter_table_output tbody tr").each(function () {

       // isList.push(this.id);
     //  alert(this.name);
     //alert( $.trim($(this).find('td:eq(0)').html()));
     var managerId = $.trim($(this).find('td:eq(0)').html());
           // isList=[];
        if(value=='R'){
          $('#status-'+this.id).val('Removal');
        } else if(value=='NC'){
          $('#status-'+this.id).val('No Change');
        } else if(value=='L'){
          $('#status-'+this.id).val('Leaver');
        } else if(value=='NA'){
          $('#status-'+this.id).val('No Longer the Manager for User');
        }
        // else {
        //   this.products =  [
        //     { "id": 1, "name": "Not Recertified" },
        //     { "id": 2, "name": "No Change" },
        //     { "id": 3, "name": "Leaver" },
        //     { "id": 4, "name": "Removal" },
        //     { "id": 5, "name": "Moved to Special Cycle" },
        //     { "id": 6, "name": "No Longer Assigned" }
        //   ];
        // }
        var data = {
          "id":managerId,
          "status":$('#status-'+this.id).val()
        };
        isList.push(data);
      });
     this.idList.push(isList);

     for (let i in this.primarymanagers) {
      for(let j in this.idList[0]){
         if(this.idList[0][j].id==this.primarymanagers[i]._id){
                this.primarymanagers[i].status = this.idList[0][j].status// here we are changing the status of the record   
            }
      }
    }
    /*if(value=='R')
    {
      //alert("seletced Removals");
      //this.products = [{ "id": 4, "name": "Removals" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'Removal';
      }

    }
    else if(value=='NC')
    {
      //alert("seletced No Change");
      //this.products = [{ "id": 2, "name": "No Change" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'No Change';
      }
    }
    else if(value=='L')
    {
      //alert("seletced Leavers");
      //this.products = [  { "id": 3, "name": "Leavers" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'Leaver';
      }
    }
    else if(value=='NA')
    {
      //alert("seletced Leavers");
      //this.products = [  { "id": 3, "name": "Leavers" }];
      for (let i in this.primarymanagers) {
        this.primarymanagers[i].status = 'No Longer the Manager for User';
      }
    }
    else
    {
      //alert("seletced None");
      this.products =  [
            { "id": 1, "name": "Not Recertified" },
            { "id": 2, "name": "No Change" },
            { "id": 3, "name": "Leaver" },
            { "id": 4, "name": "Removal" },
            { "id": 5, "name": "Moved to Special Cycle" },
            { "id": 6, "name": "No Longer Assigned" }
          ];
    }*/
}


showSelectValue(mySelect) {
    console.log(mySelect);
}

clearSearch()
{
  window.location.reload();
}


  //id = this.route.snapshot.params['_id'];
  //this.selectedProduct = this.route.snapshot.params['id'];
  //primarymanagersList : ({});
  saveResponse(){
    console.log("in save response");
    console.log(this.primarymanagers);

    if(confirm("Are you sure want to submit responses ")) {
          console.log("Implement delete functionality here");

    //this.managerextractService.updateResponses(this.id,this.primarymanagers)
      //     .subscribe(()=> this.goBack());

    for (let i in this.primarymanagers) {
            var update = {
                _id: this.primarymanagers[i]._id,
                manager: this.primarymanagers[i].manager,
                resource : this.primarymanagers[i].resource,
                initials : this.primarymanagers[i].initials,
                logonid: this.primarymanagers[i].logonid,
                userlocation: this.primarymanagers[i].userlocation,
                application: this.primarymanagers[i].application,
                status:  this.primarymanagers[i].status,// here we are changing the status of the record 
                dmanager: this.primarymanagers[i].dmanager,
                comments: this.primarymanagers[i].comments
              }

      if(update!=null && update!=undefined){
        this.managerextractService.updateResponses(update._id,update)
        .subscribe(()=> this.goBack());
      }
    }
    this.flashMessage.show('Status submitted successfully', {cssClass: 'alert-success', timeout: 1000});
           //this.router.navigate(['/output']);
            //window.location.reload();
  }

  }

  goBack(){
  (<HTMLInputElement>document.getElementById('submit')).disabled=true;
  (<HTMLInputElement>document.getElementById('clear')).disabled=true;
  (<HTMLInputElement>document.getElementById('export')).disabled=true;
  window.setTimeout(this.refreshPage,10000);
  }

  refreshPage()
  {
  location.reload(true);
  }



  //goBack(){
    //window.location.reload();
      //window.location.reload();
        //this.flashMessage.show('Status submitted successfully', {cssClass: 'alert-success', timeout: 1000});
  //this.router.navigate(['output'])

         //this.router.navigate(['/output']);
          //window.location.reload();
  //}





  getManagerExtracts(){
      this.managerextractService.getManagerExtracts()
          .subscribe(primarymanagers=>{
            this.primarymanagers = primarymanagers;
            this.filteredToatal = this.primarymanagers.length;
        });
       
          

        
    }

    public items:Array<string> = ['Not Recertified','Leavers','Removals','No Change'];

    private value:any = {};
  private _disabledV:string = '0';
  private disabled:boolean = false;

  private get disabledV():string {
    return this._disabledV;
  }

  private set disabledV(value:string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }

  public selected(value:any):void {
    console.log('Selected value is: ', value);
  }

  public removed(value:any):void {
    console.log('Removed value is: ', value);
  }

  public typed(value:any):void {
    console.log('New search input: ', value);
  }

  public refreshValue(value:any):void {
    this.value = value;
  }

  exportToExcel(event) {
    var json = [];
       for(let i in this.primarymanagers){
        var update = {
          Manager: this.primarymanagers[i].manager,
          Resource : this.primarymanagers[i].resource,
          Logonid : this.primarymanagers[i].logonid,
          User_Location: this.primarymanagers[i].userlocation,
          Application: this.primarymanagers[i].application,
          Status: this.primarymanagers[i].status,
          Deligate_Manager: this.primarymanagers[i].dmanager,
          Comments: this.primarymanagers[i].comments
         }
         json.push(update);
       }
    this.excelService.exportAsExcelFile(json, 'Recertification_Response');
  }

}
