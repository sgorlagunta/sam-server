import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsshowComponent } from './applicationsshow.component';

describe('ApplicationsshowComponent', () => {
  let component: ApplicationsshowComponent;
  let fixture: ComponentFixture<ApplicationsshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
