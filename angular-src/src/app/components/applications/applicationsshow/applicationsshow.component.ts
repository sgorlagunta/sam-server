import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
//import component, ElementRef, input and the oninit method from angular core
import { Component, OnInit, ElementRef, Input } from '@angular/core';
//import the file-upload plugin
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
//import the native angular http and respone libraries
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-applicationsshow',
  templateUrl: './applicationsshow.component.html',
  styleUrls: ['./applicationsshow.component.css']
})
export class ApplicationsshowComponent implements OnInit {

  constructor(public apiService:ApiService,
    public route:ActivatedRoute,
    public router:Router) { }

  ngOnInit() {
    this.getApplication();
  }

  primaryapplications:Object;
  getApplication(){
    var id = this.route.snapshot.params['id'];
    this.apiService.getApplication(id)
        .subscribe(primaryapplications=>{
          this.primaryapplications = primaryapplications;
        })
  }
  goBack(){
  this.router.navigate(['applications'])
  }


}
