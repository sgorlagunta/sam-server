import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
//import component, ElementRef, input and the oninit method from angular core
import { Component, OnInit, ElementRef, Input } from '@angular/core';
//import the file-upload plugin
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
//import the native angular http and respone libraries
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import {FlashMessagesService} from 'angular2-flash-messages';
import { Applications } from '../../../models/applications';
import {BU} from '../../../models/bu';
import {Platform} from '../../../models/platform';
import {Classification} from '../../../models/classification';
import {Scope} from '../../../models/scope';
import {SelectModule} from 'angular2-select';

@Component({
  selector: 'app-applicationsedit',
  templateUrl: './applicationsedit.component.html',
  styleUrls: ['./applicationsedit.component.css']
})
export class ApplicationseditComponent implements OnInit {

  constructor(public apiService:ApiService,
    public route:ActivatedRoute,
    public router:Router) { }

  ngOnInit() {
    this.getApplication();
  }
  public bu: BU[] = [
        { "id": 1, "name": "UKL" },
        { "id": 2, "name": "UKGI" },
        { "id": 3, "name": "GCUK" },
        { "id": 4, "name": "Global Life" }
      ];
  public platform: Platform[] = [
        { "id": 0, "name": "Mainframe" },
        { "id": 1, "name": "Java" },
        { "id": 2, "name": ".Net" },
        { "id": 3, "name": "3rd Party" },
        { "id": 4, "name": "Lotus Notes" },
        { "id": 5, "name": "Server" }
      ];
  public classification: Classification[] = [
        { "id": 0, "name": "Confidential" },
        { "id": 1, "name": "Highly Confidential" },
        { "id": 2, "name": "Internal Use Only" }
    ];

    public scope: Scope[] = [
            { "id": 0, "name": "Primary & Special" },
            { "id": 1, "name": "Primary Only" },
            { "id": 2, "name": "Special Only" },
            { "id": 3, "name": "Out of scope" }
        ];

  title = 'Inscope Applications';
  primaryapplications = new Applications;


  id = this.route.snapshot.params['id'];
  getApplication(){
      this.apiService.getApplication(this.id)
          .subscribe(primaryapplications=>{
            this.primaryapplications = primaryapplications;
          });
    }

    updateApplication(){
      this.apiService.updateApplication(this.id,this.primaryapplications)
          .subscribe(()=> this.goBack());
    }

     goBack(){
      this.router.navigate(['/applications'])
    }

}
