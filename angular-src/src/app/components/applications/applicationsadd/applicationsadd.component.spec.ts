import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationsaddComponent } from './applicationsadd.component';

describe('ApplicationsaddComponent', () => {
  let component: ApplicationsaddComponent;
  let fixture: ComponentFixture<ApplicationsaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
