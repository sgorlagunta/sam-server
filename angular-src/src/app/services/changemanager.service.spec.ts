import { TestBed, inject } from '@angular/core/testing';

import { ChangemanagerService } from './changemanager.service';

describe('ChangemanagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChangemanagerService]
    });
  });

  it('should be created', inject([ChangemanagerService], (service: ChangemanagerService) => {
    expect(service).toBeTruthy();
  }));
});
