import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map' ;

@Injectable()
export class DashboardService {

  constructor( private http:Http) { }
  getUsersDashboard(){
      return this.http.get("http://localhost:8080/api/dashboard")
          .map(res => res.json());
    }

    getManagerDashboard(){
        return this.http.get("http://localhost:8080/api/mdashboard")
            .map(res => res.json());
      }
    getManagerExtracts(manager){
      console.log("from service::"+manager);
        return this.http.get("http://localhost:8080/api/mdashboard/"+manager)
            .map(res => res.json());
      }

}
