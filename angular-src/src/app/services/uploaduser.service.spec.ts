import { TestBed, inject } from '@angular/core/testing';

import { UploaduserService } from './uploaduser.service';

describe('UploaduserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploaduserService]
    });
  });

  it('should be created', inject([UploaduserService], (service: UploaduserService) => {
    expect(service).toBeTruthy();
  }));
});
