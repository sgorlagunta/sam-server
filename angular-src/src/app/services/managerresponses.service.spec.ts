import { TestBed, inject } from '@angular/core/testing';

import { ManagerresponsesService } from './managerresponses.service';

describe('ManagerresponsesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagerresponsesService]
    });
  });

  it('should be created', inject([ManagerresponsesService], (service: ManagerresponsesService) => {
    expect(service).toBeTruthy();
  }));
});
