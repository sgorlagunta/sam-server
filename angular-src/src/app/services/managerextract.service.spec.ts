import { TestBed, inject } from '@angular/core/testing';

import { ManagerextractService } from './managerextract.service';

describe('ManagerextractService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagerextractService]
    });
  });

  it('should be created', inject([ManagerextractService], (service: ManagerextractService) => {
    expect(service).toBeTruthy();
  }));
});
