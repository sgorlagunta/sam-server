import { TestBed, inject } from '@angular/core/testing';

import { MloginService } from './mlogin.service';

describe('MloginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MloginService]
    });
  });

  it('should be created', inject([MloginService], (service: MloginService) => {
    expect(service).toBeTruthy();
  }));
});
