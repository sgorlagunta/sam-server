import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map' ;

@Injectable()
export class ManagerextractService {

  constructor( private http:Http) { }
  getManagerExtracts(){
      return this.http.get("http://localhost:8080/api/managerextract")
          .map(res => res.json());
    }

    getManagers(){
        return this.http.get("http://localhost:8080/api/managerchange")
            .map(res => res.json());
      }

    getManager(id){
          return this.http.get("http://localhost:8080/api/managerchange/"+id)
              .map(res => res.json());
        }
    updateManager(id, info){
          return this.http.put("http://localhost:8080/api/managerchange/"+id,info)
              .map(res => res.json());
        }
     updateResponses(id,info){
       console.log("in service function of update response");
       console.log("in service function of update response"+id,info);
         return this.http.post("http://localhost:8080/api/manageroutput/"+id,info)
             .map(res => res.json());
     }
     getManagerExtractsUsers(){
         return this.http.get("http://localhost:8080/api/userdelegate")
             .map(res => res.json());
       }




  }
