const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const ManagerExtractsSchema = mongoose.Schema({
  manager: {
    type: String
  },
  resource: {
    type: String
  },
  initials: {
    type: String
  },
  logonid: {
    type: String
  },
  userlocation: {
    type: String
  },
  application: {
    type: String
  },
  status: {
    type: String
  },
  dmanager: {
    type: String
  },
  comments: {
    type: String
  }
  },
    { versionKey: false });


const ManagerExtract = module.exports = mongoose.model('managerextracts', ManagerExtractsSchema);

module.exports.getManagerExtracts = function(callback){
  // need to ask ravi for the query
  //ManagerExtract.find({ }, { },callback).sort({manager:1});//new one copied from manageralextract
   ManagerExtract.find({'status':'No Longer the Manager for User'}, { },callback);// old one not getting the data for assignmanager option

        //UserExtract.find({ }, { logonid: 1, application: 1, region: 1, name: 1, _id: 0 },callback);
}
module.exports.addManagers = function(newUploadusers, callback){
    ManagerExtract.create(newUploadusers, callback);
}
module.exports.updateManager = function(id, newUploadusers, callback){
    ManagerExtract.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.deleteManager = function(id, callback){
    ManagerExtract.findByIdAndRemove(id, callback);
}
module.exports.getManager = function(id, callback){
    ManagerExtract.findById(id, callback);
}
module.exports.getManagers = function(callback){
    ManagerExtract.distinct('manager',callback);
}
module.exports.getManagersName = function(manager,callback){
    ManagerExtract.find({'manager':manager},callback);
}
module.exports.updateManagerName = function(id, newUploadusers, callback){
    ManagerExtract.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.updateResponses = function(id, newApplications, callback){
    ManagerExtract.findByIdAndUpdate(id, newApplications, callback);
}

module.exports.getUsersDashboard = function(callback){
    ManagerExtract.find(callback);
    //ManagerExtract.distinct('application',callback);
}


//module.exports.updateManagerName = function(oldname, update, callback){
  //  ManagerExtract.updateMany({"manager":oldname},{$set : {"manager":update.manager}});

    //updateMany({"manager":"ACCENTURE@csc.com"},{$set : {"manager":"accenture.csc.com"}})
//}
