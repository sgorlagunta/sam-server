const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User Schema
const UploadUsersSchema = mongoose.Schema({
  logonid: {
    type: String
  },
  application: {
    type: String
  },
  region: {
    type: String
  },
  name: {
    type: String
  }
  },
    { versionKey: false });


const UploadUsers = module.exports = mongoose.model('uploadusers', UploadUsersSchema);

module.exports.getUploadusers = function(callback){
    UploadUsers.find(callback);
}
module.exports.addUploadusers = function(newUploadusers, callback){
    UploadUsers.create(newUploadusers, callback);
}
module.exports.updateUploadusers = function(id, newUploadusers, callback){
    UploadUsers.findByIdAndUpdate(id, newUploadusers, callback);
}
module.exports.deleteUploadusers = function(id, callback){
    UploadUsers.findByIdAndRemove(id, callback);
}
module.exports.getUploaduser = function(id, callback){
    UploadUsers.findById(id, callback);
}
